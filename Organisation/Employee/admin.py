from django.contrib import admin
from .models import Employee, Designation

# Register your models here.
admin.site.register((Employee, Designation))