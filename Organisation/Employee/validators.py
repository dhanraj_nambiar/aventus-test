from django.core.exceptions import ValidationError

def validate_file_size(file):
    filesize= file.size
    if filesize > 5000000:
        raise ValidationError("You cannot upload file more than 10Mb")
    else:
        return filesize