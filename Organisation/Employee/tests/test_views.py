import random
from django.test import TestCase

from Employee.models import Employee,Designation

class EmployeeListViewTest(TestCase):
    def setUp(self):
        desigs = ['Sr Dev', 'Lead Dev', 'Jr Dev']
        for des in desigs:
            Designation.objects.create(name=des)
        designtns = list(Designation.objects.all())
        for i in range(20):
            Employee.objects.create(username='Employee'+str(i),Address='Test',
            Designation=random.choice(designtns), email='Employee'+str(i)+'@ok.com')

    def test_list_view(self):
        resp = self.client.get('/employee/employee_list/')
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp.context_data['object_list']), 5)