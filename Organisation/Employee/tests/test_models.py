from django.test import TestCase

from Employee.models import Employee, Designation
# Create your tests here.

class EmployeeTestCase(TestCase):
    def setUp(self):
        des1 = Designation.objects.create(name='Sr Dev')
        des2 = Designation.objects.create(name='Lead Dev')
        Employee.objects.create(username='TonyJha', Address='2nd avnue, Chennai',
        Designation=des1, email='tony@jha.com')
        Employee.objects.create(username='NaveenKK',Address='Palayam, Calicut',
        Designation=des2, email='naveen@kk.com')
        Employee.objects.create(username = 'ManuJohn', Address='Kannur', 
        Designation=des1, email='manu@john.com')

    def test_for_equivalence(self):
        emp1 = Employee.objects.get(username='TonyJha')
        emp2 = Employee.objects.get(username='NaveenKK')
        emp3 = Employee.objects.get(username='ManuJohn')
        self.assertEqual(Employee.is_buddies(emp1, emp2), False)
        self.assertEqual(Employee.is_buddies(emp1,emp3), True)

    def test_for_username(self):
        emps = Employee.objects.all()
        res = [emp.username == emp.first_name + emp.last_name for 
        emp in emps]
        self.assertEqual(all(res), True)
