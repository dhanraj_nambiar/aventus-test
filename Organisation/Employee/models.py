from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.
from .validators import validate_file_size

class Designation(models.Model):
    name = models.CharField(max_length=50,unique=True)

    def __str__(self) -> str:
        return self.name


class Employee(AbstractUser):
    Address = models.TextField()
    Photo = models.ImageField(null=True, blank=True, upload_to=\
            'employee/profile_img/', validators=[validate_file_size])
    Designation = models.ForeignKey(Designation, null=True, on_delete=\
                                                        models.SET_NULL)
    
    def __str__(self) -> str:
        return f"{self.first_name} {self.last_name}"

    @staticmethod
    def is_buddies(emp1, emp2):
        return emp1.Designation.name == emp2.Designation.name

    class Meta:
        unique_together = ('email',)