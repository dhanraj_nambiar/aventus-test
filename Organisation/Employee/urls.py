from django.urls import path, re_path

from .views import *

urlpatterns = [
    path('add_employee/', AddEmployee.as_view()),
    path('employee_view/', EmployeeView.as_view()),
    path('employee_list/', EmployeeListView.as_view()),
    path('employee_update/<int:pk>/', EmployeeUpdateView.as_view()),
    path('employee_delete/<int:pk>/', EmployeeDeleteView.as_view()),
    path('login/', AdminLogin.as_view(), name='login'),
    path('logout/', AdminLogout.as_view(), name='logout')
]