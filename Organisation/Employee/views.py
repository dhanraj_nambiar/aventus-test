from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views import View
from django.views.generic import ListView
from django.views.generic.edit import UpdateView, DeleteView
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.mixins import LoginRequiredMixin
# Create your views here.

from .models import Designation,Employee
from .forms import EmployeeForm


class EmployeeListView(ListView):
    model = Employee
    paginate_by = 5

class AddEmployee(LoginRequiredMixin,View):
    def post(self, request):
        form = EmployeeForm(request.POST)
        if form.is_valid():
            try:
                form.save()
            except Exception as e:
                return render(request, 'Employee/add_employee.html', {'form':form, \
                'msg':str(e)})
            else:
                return HttpResponseRedirect('/employee/employee_list')
        else:
            return render(request, 'Employee/add_employee.html', {'form':form,'msg':\
            "Bad data"})

    def get(self, request):
        form = EmployeeForm()
        return render(request, 'Employee/add_employee.html', {'form':form})

class EmployeeView(LoginRequiredMixin, View):
    def get(self, request):
        emp_id = request.GET['number']
        try:
            emp = Employee.objects.get(pk = emp_id)
        except Exception as e:
            return render(request, 'Employee/employee_list.html', {'msg':str(e)})
        else:
            return render(request, 'Employee/employee_home.html',{'employee':emp})

class EmployeeUpdateView(LoginRequiredMixin,UpdateView):
    model = Employee
    fields = ["first_name", "last_name", "email","Address", "Designation", "Photo"]
    template_name_suffix = "_update_form"
    success_url = '/employee/employee_list/'

class EmployeeDeleteView(LoginRequiredMixin,DeleteView):
    model = Employee
    success_url = '/employee/employee_list/'

class AdminLogin(LoginView):
    template_name = 'Employee/login.html'

class AdminLogout(LogoutView):
    next_page = '/employee/employee_list'