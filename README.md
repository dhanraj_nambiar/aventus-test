# Aventus Test
## Name
Django Project for a test purpose

## Description
An Employee app with Employee model with provision for adding Employees, updating Name, Photo, Designation, Address. Listing view is public, Employee data updating and deleting is only for admin. General Login and Logout functionalities exist. API for Registering new Users is not added since only a single User needed to test functionality(Admin user can be added using 'python manage.py createsuperuser' command).


## Installation
git clone https://gitlab.com/dhanraj_nambiar/aventus-test.git .
virtualenv  env_name
source env_name/bin/activate
cd Organisation/
pip install -r requirements.txt
python manage.py makemigrations
python manage.py migrate
python manage.py collectstatic
python manage.py createsuperuser
python manage.py runserver
